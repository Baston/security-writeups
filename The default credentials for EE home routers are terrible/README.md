# The default credentials for EE home routers are terrible.
###### This is written for educational purposes only.
## Introduction
You should always change the default user name and password on any device that comes with one.

In this report I will go over my research and findings on this topic and further demonstrate how bad it actually is.

Over the period of six months I received five EE home Wi-Fi routers and I noticed that they all had similar default passwords set for the Wi-Fi network. These routers consisted of the EE BrightBox 1, 2 and the EE Smart Hub. I conducted some further research and found that the default passwords were terrible and need to be improved.

This project was made and designed to be tested on Arch Linux / Debian Linux. It has so far not been tested on any other OS and may require some additional dependencies such as Python3 and a variety of other security tools.

## Research

### My Experience

I first noticed that the passwords followed the pattern of 3 words separated by "-". Each word was either three four or five words. Below is a table of what passwords were used by what device.

| Router       | Password       |
| :----------: | :------------: |
| BrightBox 1  | mop-cross-dear |
| BrightBox 2  | aim-store-must |
| BrightBox 2  | fame-reset-far |
| BrightBox 2  | train-apt-team |
| Smart Hub    | cord-TRY-aback |

They seem to follow a pattern of word counts where the numbers here would be: 3-5-4, 4-5-3, 5-3-4 and 4-3-5. However as you can see the last entry from the Smart Hub has a capitalised word but in my research that I will mention later I found that no mention of the BrightBoxes having any capitalised letters

### An article by The Sun

The best resource for research is always [Google][1] for me and I found something immediately. A link to an article by The Sun titled "['BOMB-TOWER-SAD' Mum’s horror at password for her new EE broadband router][2]". This article has some very relevant information in it. For one it has another password to add to our list and shows the "important bits" card where the password is kept showing that the password is lowercase. The Sun got a quote from an EE spokesperson who said

"We work with a third party to automatically generate default passwords consisting of three randomly selected words which customers use to login to their home WiFi, which is a much easier experience for our customers than a random string of letters and numbers." ...  "Our partner does have a list of inappropriate words which are ruled out of these combinations and we've asked them to urgently review this list"

This informed me that their third party uses a word list and that that list does not contain "inappropriate" words. That narrows down how many possible combinations there are for passwords. Not by much but still a small amount.

### Hashkiller Forums

The next piece of information I found was found on the Hashkiller forums. Reading through it I found that there was that someone else has discovered this earlier. I also fond some more passwords to add to the example list in the pattern section of this report. The discussion on the Hashkiller forums gave me more information about not just EE BrightBoxes but other default Wi-Fi passwords. An example of this would be that Sky use passwords 8 hexadecimal characters in length. On both these forum threads there was a lot of information and one very use full resource. The next piece of information was provided by the user "Milzo" and was that there was an active search for these passwords and that this user was compiling a word list containing all these possible passwords.

## Demonstrating The Attack

WPA2 handshakes are easy to capture and work on offline. For this demonstration I will be attacking and cracking my own Wi-Fi password which only has my own devices on it. I will also demonstrate using python to produce a word list that can be used to decrypt the handshake file.

### The Wordlist

As we know what the various possible combinations I will make a python programme to combine the words from 3 files into the correct format that being a terminal that I can pipe it into hashcat later. This is my second python project ever so expect my code to work but look terrible.

Inside the Word-list folder is a file called Example_Word_list.txt this contains all the passwords I found online while doing my testing. There are also the files Three_Letter_Words.txt, Four_Letter_Words.txt and Five_Letter_Words.txt. These files contain words from the English language of that length taken from the dictionary. Because of the article from the sun I can omit some words that may seem "controversial". There are only a few of these words however any optimisations that can be made must be used to our advantage.

I have made a Python program to generate a Word-list that gets saved to a file. I have not shipped this repository with the Word-list as it is approximately HOW BIT BOI. The file is too large for Github and very easy to generate as it takes approximately HOW LONKKKK to be crated. You can check that the file has been properly created by comparing its SHA-256 sum to the one listed at the end of this document. A SHA-256 sum can be calculated on most Linux systems by using the command "sha256sum <file>". The Word-list should be crated first as it is a prerequisite for this project.

### Acquiring the Wi-Fi handshake.

####Getting hold of a Wi-Fi handshake to crack offline is very easy and very illegal and should not be done unless in a tightly controlled environment that you own.

To hack into the Wi-Fi network a "handshake file" must be acquired. When a device connects to a Wi-Fi network it goes through a process called a handshake. To capture this handshake I'm going to use the [Airgeddon][5] suite which is a set of tools compiled by V1s1t0r to audit wireless networks. Here is a link to a YouTube video by Bull Byte on [Hacking Wi-Fi in Seconds with Airgeddon...][6]. This video will show you how to use airgeddon to capture a Wi-Fi handshake. In the video also shows you how to use the EE_Wordlist to crack the password.


THIS IS NOT FINISHED. DOMONSTRATION STILL NEEDS TO BE COMPLETE!

### SHA SUMS

EE_Wordlist.txt:


#### Special thanks to Tomas who help'd me with my code

[Link to Google][1]

[Link to The Sun "'BOMB-TOWER-SAD' Mum’s horror at password for her new EE broadband router"][2]

[Link to first Hashkiller forums][3]

[Link to second Hashkiller forums][4]

[Link to airgeddon by visitor][5]

[1]: <https://google.com> "Link to google.com"
[2]: <https://www.thesun.co.uk/news/1405013/mums-horror-at-password-for-her-new-ee-broadband-router/> "Link to the article by The Sun"
[3]: <https://forum.hashkiller.co.uk/topic-view.aspx?t=6787> "Link to Hashkiller forums"
[4]: <https://forum.hashkiller.co.uk/topic-view.aspx?t=1660&p=3> "Link o hashkiller forums"
[5]: <https://github.com/v1s1t0r1sh3r3/airgeddon> "Link to visitors Github"
[6]: <https://youtu.be/ejTPWPGP0GA?t=459> "Hacking Wi-Fi in Seconds with Airgeddon & Parrot Security OS [Tutorial] YouTube"

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#tables
